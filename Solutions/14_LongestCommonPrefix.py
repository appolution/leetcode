class Solution(object):
    # def longestCommonPrefix(self, strs):
    #     """
    #     :type strs: List[str]
    #     :rtype: str
    #     """
    #     if len(strs) == 0:
    #         return ""
    #     elif len(strs) == 1:
    #         return strs[0]
    #     else:
    #         result = strs[0]
    #         for i in range(1, len(strs)):
    #             result = self.lcp(result, strs[i])
    #         return result
    #
    # def lcp(self, short, long):
    #     if len(short) > len(long):
    #         return self.lcp(long, short)
    #     else:
    #         for i in range(0, len(short)):
    #             if short[i] != long[i]:
    #                 return short[:i]
    #         return short
    def longestCommonPrefix(self, strs):
        """
        :type strs: List[str]
        :rtype: str
        abbc
        abcc
        abaa
        """
        if not strs:
            return ""
        for index, value in enumerate(zip(*strs)):
            if len(set(value)) != 1:
                return strs[0][:index]
        return min(strs)


def execute():
    sol = Solution()
    print(sol.longestCommonPrefix(["abbc", "abcc", "abaa"]))
    # print(sol.lcp("abca", "abcc"))


execute()
