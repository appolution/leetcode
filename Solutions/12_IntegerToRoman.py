import unittest


class Solution(object):
    def intToRoman(self, num):
        """
        :type num: int
        :rtype: str
        I II III
        IV V
        VI VII VIII
        IX

        X XX XXX
        XL L
        LX LXX LXXX
        XC

        C CC CCC
        CD D
        DC DCC DCCC
        CM

        M MM MMM

        2345 --> MMCCCXLV
        1678 --> MDCLXXVIII
        内核恐慌

        -------------------------
        Kata
        -------------------------
        TDD
        Red --> Green --> Refactor
        -------------------------
        """
        ones = ['', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX']
        tens = ['', 'X', 'XX', 'XXX', 'XL', 'L', 'LX', 'LXX', 'LXXX', 'XC']
        hundreds = ['', 'C', 'CC', 'CCC', 'CD', 'D', 'DC', 'DCC', 'DCCC', 'CM']
        thousands = ['', 'M', 'MM', 'MMM']
        return thousands[num // 1000 % 10] + hundreds[num // 100 % 10] + tens[num // 10 % 10] + ones[num % 10]


class UnitTest(unittest.TestCase):
    sol = Solution()

    def test(self):
        self.assertEqual(self.sol.intToRoman(1), "I")
        self.assertEqual(self.sol.intToRoman(2), "II")
        self.assertEqual(self.sol.intToRoman(4), "IV")
        self.assertEqual(self.sol.intToRoman(5), "V")
        self.assertEqual(self.sol.intToRoman(6), "VI")
        self.assertEqual(self.sol.intToRoman(9), "IX")

        self.assertEqual(self.sol.intToRoman(10), "X")
        self.assertEqual(self.sol.intToRoman(20), "XX")
        self.assertEqual(self.sol.intToRoman(40), "XL")
        self.assertEqual(self.sol.intToRoman(50), "L")
        self.assertEqual(self.sol.intToRoman(60), "LX")
        self.assertEqual(self.sol.intToRoman(90), "XC")

        self.assertEqual(self.sol.intToRoman(100), "C")
        self.assertEqual(self.sol.intToRoman(200), "CC")
        self.assertEqual(self.sol.intToRoman(400), "CD")
        self.assertEqual(self.sol.intToRoman(500), "D")
        self.assertEqual(self.sol.intToRoman(600), "DC")
        self.assertEqual(self.sol.intToRoman(900), "CM")

        self.assertEqual(self.sol.intToRoman(1000), "M")
        self.assertEqual(self.sol.intToRoman(2000), "MM")
        self.assertEqual(self.sol.intToRoman(3000), "MMM")
        self.assertEqual(self.sol.intToRoman(1996), "MMM")


        if __name__ == '__main__':
            unittest.main()
