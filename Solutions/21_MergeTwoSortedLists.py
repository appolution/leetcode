import sys


class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

    @staticmethod
    def generate(listData):
        result = ListNode(0)
        head = None
        for item in listData:
            result.next = ListNode(item)
            if head is None:
                head = result.next
            result = result.next
        return head

    def print(self):
        header = self
        print(header.val, end="")
        while header.next is not None:
            header = header.next
            print("->", end="")
            print(header.val, end="")


class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        """
        🡧 cur1
        1 -> 2 -> 4
        1 -> 3 -> 4 -> 5
        🡤 cur2
        """
        root = ListNode(0)
        cur = root
        cur1 = l1
        cur2 = l2
        while cur1 is not None or cur2 is not None:
            val1 = cur1.val if cur1 is not None else sys.maxsize
            val2 = cur2.val if cur2 is not None else sys.maxsize
            if val1 < val2:
                cur.next = cur1
                cur1 = cur1.next
            else:
                cur.next = cur2
                cur2 = cur2.next
            cur = cur.next
        return root.next


def exec():
    sol = Solution()
    l1 = ListNode.generate([1, 2, 4])
    l2 = ListNode.generate([1, 3, 4])
    sol.mergeTwoLists(l1, l2).print()


exec()
