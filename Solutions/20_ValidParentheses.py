class Solution:
    # sol 1
    def isValid(self, s: str) -> bool:
        stack = []
        for char in s:
            if char == '(' or char == '[' or char == '{':
                stack.append(char)
            else:
                if stack:
                    top = stack.pop()
                else:
                    top = "#"
                if not ((top == '(' and char == ")") or (top == '[' and char == "]") or (top == '{' and char == "}")):
                    return False
        return not stack

    # sol 2
    # def isValid(self, s: str) -> bool:
    #     while "()" in s or "[]" in s or "{}" in s:
    #         s = s.replace("()", "").replace("[]", "").replace("{}", "")
    #     return s == ""

    # sol 3
    # def isValid(self, s: str) -> bool:
    #     stack = []
    #     dictionary = {'}': '{', ']': '[', ')': '('}
    #
    #     for char in s:
    #         if char in dictionary:
    #             if stack:
    #                 top_element = stack.pop()
    #             else:
    #                 top_element = '#'
    #
    #             if top_element != dictionary[char]:
    #                 return False
    #         else:
    #             stack.append(char)
    #     return not stack


def exec():
    sol = Solution()
    print(sol.isValid("[()]"))


exec()
