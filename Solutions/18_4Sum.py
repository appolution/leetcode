from collections import OrderedDict
from typing import List


class Solution(object):
    @staticmethod
    def nSum(n, nums: List[int], target: int) -> List[List[int]]:
        result = []
        if n == 1:
            for i in nums:
                if i == target:
                    return [[i]]
        if n == 2:
            for firstIndex in range(0, len(nums)):
                for secondIndex in range(firstIndex, len(nums)):
                    firstVal = nums[firstIndex]
                    secondVal = nums[secondIndex]
                    if firstVal + secondVal == target:
                        result.append([firstVal, secondVal])
        if n >= 3:
            for firstIndex in range(0, len(nums)):
                removedVal = nums[firstIndex]
                fixedTarget = target - removedVal
                fixedNums = nums.copy()
                fixedNums.remove(removedVal)
                fixedResult = Solution.nSum(n - 1, fixedNums, fixedTarget)
                for item in fixedResult:
                    item.append(removedVal)
                    item.sort()
                    result.append(item)
        return result

    def fourSum(self, s):
        return s


def execute():
    print(Solution.nSum(4, [1, 0, -1, 0, -2, 2], 0))


execute()
