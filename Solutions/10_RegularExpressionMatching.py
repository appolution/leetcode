import numpy


class Solution(object):
    def isMatch(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool

        c*a*b <==> aab
        ---> b==b
        c*a* <==> aa

    abcd <==> a*.cd
----------------------------
          S 0 1 2 3
      M   0 1 2 3 4
            a b c d
    P 0   T F F F F
    0 1 a F T F F F
    1 2 * T T F F F
    2 3 . F T T F F
    3 4 c F F F T F
    4 5 d F F F F T
----------------------------
          S 0 1 2 3
          0 1 2 3 4
            a b a a
    P 0   T F F F F
    0 1 a F T F F F
    1 2 b F F T F F
    2 3 . F F F T F
    3 4 * F F T T T
----------------------------

      1 Matrix --> False
      2 m[0,0] --> True
      3 for loop Pattern i position.
        p[i] == '*' --> m[i+1,0] = m[i-1, 0]
      4 for loop Matrix [i,j] position.
        if p[i - 1] == '.'
            --> m[i,j] = m[i-1,j-1] and True
        if p[i - 1] == '*'
            --> if p[i-2] != s[j-1] and p[i-2] != '.'
                    --> m[i,j] = m[i-2][j]
                else
                    --> m[i,j] = m[i-2,j] or m[i-1,j] or m[i,j-1]
        else
            --> m[i,j] = m[i-1,j-1] and p[i-1] == s[j-1]
        """

    def isMatch(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """
        x = len(s) + 1
        y = len(p) + 1
        if y == 1:
            return x == 1
        matrix = [[False] * x for i in range(y)]
        matrix[0][0] = True
        for i in range(y - 1):
            if p[i] == '*':
                matrix[i + 1][0] = matrix[i - 1][0]
        for i in range(1, y):
            for j in range(1, x):
                if p[i - 1] == '.':
                    matrix[i][j] = matrix[i - 1][j - 1]
                elif p[i - 1] == '*':
                    if p[i - 2] != s[j - 1] and p[i - 2] != '.':
                        matrix[i][j] = matrix[i - 2][j]
                    else:
                        matrix[i][j] = matrix[i - 2][j] or matrix[i - 1][j] or matrix[i][j - 1]
                else:
                    matrix[i][j] = matrix[i - 1][j - 1] and p[i - 1] == s[j - 1]
        return matrix[-1][-1]


def execute():
    sol = Solution()
    print(sol.isMatch("aab", "c*a*b*"))


execute()
