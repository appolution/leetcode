class Solution(object):
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        if x == 0:
            return True
        if x < 0 or x % 10 == 0:
            return False
        reverse = 0
        while x > reverse:
            reverse = reverse * 10 + x % 10
            x = int(x / 10)
        if x == reverse or x == int(reverse / 10):
            return True
        else:
            return False


def execute():
    sol = Solution()
    print(sol.isPalindrome(10))


execute()
