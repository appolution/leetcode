class Solution(object):
    def maxArea(self, height):
        """
        :type height: List[int]
        :rtype: int

            |
            |       |
            |       |
        |   |   |   |
        |   |   |   |   |       |
        |___|___|___|___|___|___|
        0   1   2   3   4   5   6

        n-1 + n-2 + n-3 + ... + 1   (n-1+1)(n-1)/2  O(n^2)

        O(n)
        low = 0
        high = n
        while(low < high)
            height[low] > height[high] ? high-- : low++
            max update
        """
        low = 0
        high = len(height) - 1
        maxVal = 0
        while low < high:
            lowVal = height[low]
            highVal = height[high]
            if lowVal > highVal:
                temp = highVal * (high - low)
                high -= 1
            else:
                temp = lowVal * (high - low)
                low += 1
            maxVal = max(maxVal, temp)
        return maxVal


def execute():
    sol = Solution()
    print(sol.maxArea([1, 1]))


execute()
